
function main() {

  var counter, randomX, randomY;
  
  // 3 simple instructions
  createBird(0, 10);
  createBird(150, 10);
  createBird(300, 10);

  // A "Loop"
  for(counter=0; counter<10; counter++) {
    // Another 3 instructions
    randomX = Math.random()*300;
    randomY = 80 + Math.random()*150;
    createBird(randomX, randomY);
  }
  
}
