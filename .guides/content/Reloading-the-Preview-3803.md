Press the reload button in the preview window. The preview window is the one with all the birds in it on the left. The reload button is in the top left with the circular arrows). Notice how you get new bird positions each time you press it.

![](.guides/img/reload.png)

You can also modify the code if you want to. Reload the preview again after making any changes.

Whenever you make any changes to the code, press the Reload button to see the changes.

|||info
Move to the next page by clicking the **Next** button at the top left of this page and we'll start hacking around our first piece of code.
|||

