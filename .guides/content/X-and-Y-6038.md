In the examples that follow, we'll shorten our variable names so they are a bit more concise.

- Instead of `left` we will use `x`
- Instead of `top` we will use `y`

For those of you who have already know about coordinates, sorry for explaining the obvious. If you have never heard of X and Y coordinates, now you have!

So instead of ...

```javascript
function main() {

  var pixelsFromTheLeft, pixelsFromTheTop;
  
  pixelsFromTheLeft = 250;
  pixelsFromTheTop = 10;
  
  positionBigBird(pixelsFromTheLeft, pixelsFromTheTop);
  
}
```

We now will use

```javascript
function main() {

  var x, y;
  
  x = 250;
  y = 10;
  
  positionBigBird(x, y);
  
}
```
