Look at `main.js` on the left to view your code.

|||challenge
Can you see how to sit both birds on the branch by adjusting the 2 variables `pixelsFromTheLeft` and `pixelsFromTheTop` coordinates in `main.js` on the left? 

Press the reload button in the Preview tab after editing the code.

![](.guides/img/var-bird-positioned.png)
|||

We'll describe what's going on in the next section.
