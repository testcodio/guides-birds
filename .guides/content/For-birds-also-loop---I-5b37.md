We'll now introduce another kind of loop - the *for* loop. The for loop does exactly the same thing as the *while* loop, but it does it in a slightly different way. The for loop is generally preferred to *while* loops in Javascript.

The *while* loop statement just deals with the condition (`while(counter<7)` in our previous example).

||| definition
The *for* loop statement allows you to handle the following things

1. The initialization of your variables.
1. The condition.
1. What happens at the very end of the loop.

|||

## Video
Let's have a look at a video that helps you visualise what's going on with our loop.

<div>
  <iframe src="//player.vimeo.com/video/119145833" width="500" height="330" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>