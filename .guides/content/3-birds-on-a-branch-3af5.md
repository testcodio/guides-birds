Now take a look at the new code that's opened up on the left. Click on the `main.js` tab to see it. We've got 3 birds this time but they're not roosting nicely.

|||challenge
## Challenge
You should get all 3 birds to sit on the branch like this.

![](.guides/img/r-bird-branch3-1.png)

After you have changed the code, be sure to press the reload button in the Preview window.
|||

It's not that exciting but you should note how it is maybe a bit tedious setting the x and y position (coordinate) of all 3 birds.

In the next section, we'll explain how to do this another way.