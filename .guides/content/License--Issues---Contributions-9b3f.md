## Commenting & contributing content
If you are interested in making any suggestions, comments or actual content contributions or corrections to this Guide, then please visit [this page](https://codio.com/docs/ide/tools/guides/contributions/) for more information on how to do this.

## Guide Repository & Issues
We manage issues through the BitBucket repository. You will need a BitBucket account if you wish to make comments or contribute content. This is quick and free of charge. If you click on a link below you will be asked to sign up.

**Issues & Comments** : https://bitbucket.org/codiocontent/introduction-to-coding/issues?status=new&status=open
**Repository** :  https://bitbucket.org/codiocontent/introduction-to-coding

## Copyright notice
Copyright © 2015 by Codio Ltd.

All rights reserved. No part of this content may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, or other electronic or mechanical methods, without the prior written permission of the publisher, except in the case of brief quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright law. If you clone, fork or otherwise copy this content then you must leave this copyright notice in place and clearly display it. For permission requests, please email help@codio.com or submit your request in writing to the address below.

Codio Ltd.
29 Wood Street
Stratford-upon-Avon
CV37 6JG