|||definition
A program is an organised list of instructions that are executed in a specific, predominantly sequential manner. Without a program, a computer is useless.
|||

### And what is "coding"?

|||definition
Coding is another word for programming. Code means lines of programming code. You will commonly hear developers talk about 'code' and 'coding' rather than 'program' and 'programming'.
|||

## See some code in action
Take a look at the video below and you can see an example piece of code that illustrates the idea. Don't worry about the details, we'll be explaining those during the rest of this Guide.

<div>
  <iframe src="//player.vimeo.com/video/121818701" width="500" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>


