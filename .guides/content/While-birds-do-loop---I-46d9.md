Here comes a small jump, so brace yourself. We're now going to learn about *loops*. 

Loops are essential in all programming languages, so let's dive in and what a loop is.

In fact, in the very first section, we used a loop, so let's take a look.

## Video explanation

<div>
  <iframe src="//player.vimeo.com/video/121280245" width="500" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

|||definition
## Counters
Counters are used a *lot* with loops. Counters keep track of the number of times your code goes round the loop. Try to fully grasp 

- initialising the counter (`counter=0`)
- incrementing (increasing the value of) the counter
- how the ctr keeps in incrementing until an upper limit is reached

|||
