# An Introduction to Coding
This Guide is designed to introduce you to the general principles of programming. This introduction does not aim to teach you how to code - that comes in another module. The idea is to expose you to the types of things you will find in pretty well all programming languages.

|||
## What we'll cover in this Guide
- Variables
- Loops
- If, then, else statements (know as conditional statements)
- Functions
- Flowcharts and algorithms

|||

We are using Javasript for this but the principles explained here apply to almost all programming languages. 

You will be encouraged to make code changes and then run them. There are also videos to help explain some of the more visual concepts.

Once you have completed this Guide, you can move on and start learning more formally.

Have fun!

## Using a Codio Guide

There is not much you need to know, but here are a few things you might want to know.

- You can move forwards and backwards one page at a time using the arrow buttons in the left of the header bar at the top.
- You can see the table of contents by pressing the  hamburger icon on right of the header bar at the top.
- If you stop the Guide, you can restart it at any time from the Tools->Guides->Play menu item.

![](https://codio-public.s3.amazonaws.com/welcome-instructions.png)

## Comments, contributions and copyright
If you want more information about how to provide feedback, add your own suggestions and contributions or copyright information, open the tabel of contents and head to the last section 'License, Issues & Contributions'.